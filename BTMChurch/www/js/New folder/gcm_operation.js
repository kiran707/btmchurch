     var ATTACHMENT_URL = 'http://teknikabloodapp1.no-ip.biz:8078/BTMChurch/data/';
     
     // Variables used for database operations  
     		var device_id;
            var status=0;
			var title;
			var date;
			var thought;
			var attachment_url;
			var img_name;
			var flag = "false";
			var imgpath;
			var pdfpath;
			var frmtime;
			var totime; 
     
     
     		var infid;
            var pushNotification;
            
            
            function onDeviceReady() {
           
			
				try 
				{ 
                	pushNotification = window.plugins.pushNotification;
                	if (device.platform == 'android' || device.platform == 'Android' ||
                            device.platform == 'amazon-fireos' ) {
			pushNotification.register(successHandler, errorHandler, {"senderID":"687734401229","ecb":"onNotification"});		 
					} else {
                    	pushNotification.register(tokenHandler, errorHandler, {"badge":"true","sound":"true","alert":"true","ecb":"onNotificationAPN"});	// required!
                	}
                }
				catch(err) 
				{ 
					txt="There was an error on this page.\n\n"; 
					txt+="Error description: " + err.message + "\n\n"; 
					alert(txt); 
				} 
            }
            
            // handle APNS notifications for iOS
            function onNotificationAPN(e) {
                if (e.alert) {
                     $("#app-status-ul").append('<li>push-notification: ' + e.alert + '</li>');
                     // showing an alert also requires the org.apache.cordova.dialogs plugin
                   
                     navigator.notification.alert(e.alert);
                      
                
                }
                    
                if (e.sound) {
                    // playing a sound also requires the org.apache.cordova.media plugin
                    var snd = new Media(e.sound);
                    snd.play();
                }
                
                if (e.badge) {
                    pushNotification.setApplicationIconBadgeNumber(successHandler, e.badge);
                }
            }
            
            
            // handle GCM notifications for Android
            function onNotification(e) {
               // $("#app-status-ul").append('<li>EVENT -> RECEIVED:' + e.event + '</li>');
			   
			   alert(e);              //CHANGE
              
                switch( e.event )
                {
                    case 'registered':
					if ( e.regid.length > 0 )
					{
						console.log("regID = " + e.regid);
						device_id = e.regid;
						window.localStorage.setItem("mySum", device_id);	
						
						//Save device id in local database
						saveDevID();
					}
					
                    break;
                    
                    case 'message':
                    	// if this flag is set, this notification happened while we were in the foreground.
                    	// you might want to play a sound to get the user's attention, throw up a dialog, etc.
                    	
					              		alert(JSON.stringify(e));    //CHANGE
					              		//Extracting data to store into local database
					              		title 	= e.payload.message.infType;
					               		date	=	e.payload.message.infDate;
   	 									description	=	e.payload.message.description;
   	 									img_name = e.payload.message.imgPath;
   	 									pdfpath = e.payload.message.pdfPath;
   	 									frmtime = e.payload.message.frmTime;
   	 									totime  = e.payload.message.toTime;
   	 									infid = e.payload.message.infId;
										
   	 									alert(title+" "+date+" "+description+" "+img_name+" "+pdfpath+" "+frmtime+" "+totime+" "+infid);   //CHANGE
   	 									//function check for duplicate records and save the above details in local db
   	 									getRecordID(infid,date);
   	 									//saveData(infid,title,date,description,img_name,pdfpath,frmtime,totime);
                    					
                    					
                   
                    	if (e.foreground)
                    	{
							$("#app-status-ul").append('<li>--INLINE NOTIFICATION--' + '</li>');
						      
						        // on Android soundname is outside the payload. 
					            // On Amazon FireOS all custom attributes are contained within payload
					               
					          var soundfile = e.soundname || e.payload.sound;
					          // if the notification contains a soundname, play it.
					               
					         // playing a sound also requires the org.apache.cordova.media plugin
					         var my_media = new Media("/android_asset/www/"+ soundfile);
					              
					        //Play notification sound after saving the records into database   
							my_media.play();
						
						}
						else
						{	// otherwise we were launched because the user touched a notification in the notification tray.
							if (e.coldstart){
								$("#app-status-ul").append('<li>--COLDSTART NOTIFICATION--' + '</li>');
								 
								
								}
							else{
							
						 
								$("#app-status-ul").append('<li>--BACKGROUND NOTIFICATION--' + '</li>');
								 
							}
						}
							
						$("#app-status-ul").append('<li>MESSAGE -> MSG: ' + e.payload.message + '</li>');
                        
                        //android only
						$("#app-status-ul").append('<li>MESSAGE -> MSGCNT: ' + e.payload.msgcnt + '</li>');
                        
                        //amazon-fireos only
                        $("#app-status-ul").append('<li>MESSAGE -> TIMESTAMP: ' + e.payload.timeStamp + '</li>');
                    break;
                    
                    case 'error':
						$("#app-status-ul").append('<li>ERROR -> MSG:' + e.msg + '</li>');
                    break;
                    
                    default:
						 
						$("#app-status-ul").append('<li>EVENT -> Unknown, an event was received and we do not know what it is</li>');
                    break;
                }
            }
            
            function tokenHandler (result) {
                $("#app-status-ul").append('<li>token: '+ result +'</li>');
                // Your iOS push server needs to know the token before it can push to this device
                // here is where you might want to send it the token for later use.
            }
			
            function successHandler (result) {
                $("#app-status-ul").append('<li>success:'+ result +'</li>');
            }
            
            function errorHandler (error) {
                $("#app-status-ul").append('<li>error:'+ error +'</li>');
            }
            
			document.addEventListener('deviceready', onDeviceReady, true);

    
    
    //***********************************INSERT INTO DATABASE******************************************************************
	
	//create table and insert some record
    function populateDB(tx) {
    var artidate = localStorage.getItem("artidate");
    if(status == 0){  // if to avoid insertion of same record repeatedly 
        tx.executeSql('CREATE TABLE IF NOT EXISTS Artifacts (INF_ID INTEGER,ARTI_TITLE TEXT,ARTI_DATE TEXT, ARTI_DESCRIP TEXT, IMG_NAME TEXT,PDF_NAME TEXT,FRM_TIME TIME, TO_TIME TIME)');
       
        tx.executeSql("INSERT INTO Artifacts(INF_ID,ARTI_TITLE,ARTI_DATE,ARTI_DESCRIP,IMG_NAME,PDF_NAME,FRM_TIME,TO_TIME) VALUES (?,?,?,?,?,?,?,?)",  [infid,title,artidate,description,img_name,pdfpath,frmtime,totime]);
         
         window.location.href="home.html";
         // alert("Saved successfully");
          status = status+1;
          }
          
               
    }
 
    //function will be called when an error occurred
    function errorDB(err) {
       // alert("Error processing SQL: "+err.code);
    }
 
    //function will be called when process succeed
    function successDB() {
        //alert("successfully inserted!");
        //db.transaction(queryDB,errorCB);
    }

 
    /**************************************Save image to SD card*************************/
  
 window.appRootDirName = "download_test";

function download_save_attamnt(url,img_name) {
	console.log("file system is ready");
	
	//alert("attachment_url : " +img_name);
	
	//Get local file system
	window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, gotFS, fail);
	var fileTransfer = new FileTransfer();
	var filePath = "/sdcard/BTMCHURCH/"+img_name;
	
	//alert("filePath : " +filePath);
	
	//alert("Saving");

	fileTransfer.download(
	    url,
	    filePath,
	    function(entry) {
	        //alert("download complete: ");
	    },
	    function(error) {
	        alert("download error" + error.source);
	    }
	);

}

/****************************Download Pdf************************************/
function download_save_pdf(url,pdfName) {
	console.log("file system is ready");
	
	//alert("attachment_url : " +pdfName);
	
	//Get local file system
	window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, gotFS, fail);
	var fileTransfer = new FileTransfer();
	var filePath = "/sdcard/BTMCHURCH/"+pdfName;
	
	//alert("filePath : " +filePath);
	
	//alert("Saving");

	fileTransfer.download(
	    url,
	    filePath,
	    function(entry) {
	        //alert("download complete: ");
	    },
	    function(error) {
	        alert("download error" + error.source);
	    }
	);

}


function fail() {
	console.log("failed to get filesystem");
}

function gotFS(fileSystem) {
	console.log("filesystem got");
	window.fileSystem = fileSystem;
	fileSystem.root.getDirectory(window.appRootDirName, {
		create : true,
		exclusive : false
	}, dirReady, fail);
}



function dirReady(entry) {
	window.appRootDir = entry;
	console.log("application dir is ready");
}  
    
 //TAG: Added id of artifacts 
function saveData(infid,title,d,description,img_name,pdfpath,frmtime,totime){
		alert('inside save data');  //CHANGE
		if(title === 'Thought for the day' || title === 'Bulletin' || title === 'Sermon') {
  					var path = ATTACHMENT_URL.concat(img_name);	
  						//Save to SD Card
  						download_save_attamnt(path,img_name);		
  					}
  		
  		if(title === 'Evangelism' || title=== 'Sermon' ) {
  					var path = ATTACHMENT_URL.concat(pdfpath);	
  			 
  					//Save to SD Card
  					download_save_pdf(path,pdfpath);		
  				}		
  				
  			
  			
  		//Set date to local storage to save into DB later 
		localStorage.setItem("artidate", d);
						
		//Open database in exists otherwise create new
	 	var db = window.openDatabase("DEVICEDATA", "1", "Device data", 200000); //will create database DEVICEDATA  
		db.transaction(populateDB, errorDB, successDB);
  					               

}

function saveDevID(){

//Open database in exists otherwise create new
	 	var db = window.openDatabase("DEVICEDATA", "1", "Device data", 200000); //will create database DEVICEDATA  
		db.transaction(insertID, errorDB, successDB);

}
			
//create table and insert some record
    function insertID(tx) {
        tx.executeSql('CREATE TABLE IF NOT EXISTS DeviceID (DEV_ID TEXT)');
        tx.executeSql("INSERT INTO DeviceID(DEV_ID) VALUES (?)",  [device_id]);
          //alert("Inserted successfully");
    }
			
				
/***************************************************Fetch data from database to check duplicate records******************************************/		
//Get records from local database
    function getRecordID(infid, date) {
		alert('inside get record id');        //CHANGE
		   var db = window.openDatabase("DEVICEDATA", "1", "Device data", 200000); //will create database DEVICEDATA  
		   db.transaction( function(tx){ SelectRecId(tx, infid, date) }, resError,Success );
		   
    }
    
    //Select data from local DB
    function SelectRecId(tx,infid, date){
    localStorage.setItem("date", date);
      alert('after local storage');        //CHANGE
    	tx.executeSql("select * from Artifacts WHERE INF_ID=? ", [infid], Success, resError);
    	  
    }
    
    //function will be called when query succeed
    function Success(tx,response) {
    var d = localStorage.getItem("date");
   		 alert('inside success function');    //CHANGE
       if(response.rows.length == 0){
       alert('response length 0');            //CHANGE
   	 				//Save above data into local DB
   	 				  saveData(infid,title,d,description,img_name,pdfpath,frmtime,totime);
		}else {
			//alert("Do nothing");
		}
		
	}	                
    
    //function will be called when an error occurred
    function resError(error) {
       
        console.log("Error processing SQL: "+error);
    }
 /*****************************************************************Close fetch data**********************************************************/
 
    