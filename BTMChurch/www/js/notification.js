gApp = new Array();

var ATTACHMENT_URL = 'http://teknikabloodapp1.no-ip.biz:8078/BTMChurch/data/';


var status=0;
var device_id;
var title;
var date;
var thought;
var attachment_url;
var img_name;
var flag = "false";

var imgpath;
var pdfpath;
var frmtime;
var totime;

var cordovaRef = window.PhoneGap || window.Cordova || window.cordova; // old to new fallbacks

gApp.deviceready = false;
gApp.gcmregid = '';

window.onbeforeunload  =  function(e) {
 
    if ( gApp.gcmregid.length > 0 )
    {
      //The same routines are called for success/fail on the unregister. You can make them unique if you like
      window.GCM.unregister( GCM_Success, GCM_Fail );      // close the GCM

    }
};

document.addEventListener('deviceready', function() {
   // This is the Cordova deviceready event. Once this is called Cordova is available to be used
 	// $("#app-status-ul").append('<li>deviceready event received</li>' );

  //$("#app-status-ul").append('<li>calling GCMRegistrar.register, register our Sender ID with Google</li>' );
  
   document.addEventListener("backbutton", backKeyDown, true);
   
   /*
   document.addEventListener("backbutton", function(e){
   var activePage = $.mobile.activePage.attr("id");

       if(document.getElementById('#homepage')){
           e.preventDefault();
           navigator.app.exitApp();
       }
       else {
           navigator.app.backHistory()
       }
    }, false);
   */

 
  gApp.DeviceReady = true;
	 
	
  // Some Unique stuff here,
  // The first Parm is your Google email address that you were authorized to use GCM with
  // the Event Processing rountine (2nd parm) we pass in the String name
  // not a pointer to the routine, under the covers a JavaScript call is made so the name is used
  // to generate the function name to call. I didn't know how to call a JavaScript routine from Java
  // The last two parms are used by Cordova, they are the callback routines if the call is successful or fails
  //
  
  //   what ever your GCM authorized senderId is
  //	687734401229 old project   new one261859089089
  window.plugins.GCM.register("687734401229", "GCM_Event", GCM_Success, GCM_Fail );

}, false );



function
GCM_Event(e)
{

  $("#app-status-ul").append('<li>EVENT -> RECEIVED:' + e.event + '</li>');
 
  switch( e.event )
  {
  
  case 'registered':
  //alert("Register js");
    // the definition of the e variable is json return defined in GCMReceiver.java
    // In my case on registered I have EVENT and REGID defined
    gApp.gcmregid = e.regid;
    if ( gApp.gcmregid.length > 0 )
    {
      $("#app-status-ul").append('<li>REGISTERED -> REGID:' + e.regid + "</li>");
			 
			 
			 device_id = e.regid;
				 
			//alert(device_id+ "id");	 
    }


 break

  case 'message':
    // the definition of the e variable is json return defined in GCMIntentService.java
    // In my case on registered I have EVENT, MSG and MSGCNT defined

    // You will NOT receive any messages unless you build a HOST server application to send
    // Messages to you, This is just here to show you how it might work

    
   // $("#app-status-ul").append('<li>MESSAGE -> MSG: ' + e.message + '</li>');

   	 var msg=eval("("+e.message+")");
   	 
   	 
     title=msg.infType;
   	 date=msg.infDate;
   	 description=msg.description;
   	 
   	 img_name = msg.imgPath;
   	 pdfpath = msg.pdfPath;
   	 frmtime = msg.frmTime;
   	 totime  = msg.toTime;
   	 
  
     //Show the title on notification bar
        window.plugins.statusBarNotification.notify(title, "Artifact Received");
    
  	if(title === 'Thought for the day' || title === 'Bulletin' || title === 'Sermon') {
  			var path = ATTACHMENT_URL.concat(img_name);	
  			
  			//alert("path" +path);
  			
  			//Save to SD Card
  			download_save_attamnt(path,img_name);		
  		}
  		
  	if(title === 'Evangelism' || title=== 'Sermon') {
  			var path = ATTACHMENT_URL.concat(pdfpath);	
  			
  			//alert("path" +path);
  			
  			//Save to SD Card
  			download_save_pdf(path,pdfpath);		
  		}
  
  
  
  
	 /**
	 * Code for saving the contents into local database, (sqlite database)
	 */
	  
	 //Open database in exists otherwise create new
	 var db = window.openDatabase("DEVICEDATA", "1.0", "Device data", 200000); //will create database DEVICEDATA  
	  
	 db.transaction(populateDB, errorDB, successDB);
	
    break;


  case 'error':

    $("#app-status-ul").append('<li>ERROR -> MSG:' + e.msg + '</li>');
    alert('error');
	console.log(e.msg);
    break;



  default:
    $("#app-status-ul").append('<li>EVENT -> Unknown, an event was received and we do not know what it is</li>');
		
		alert("Unknown, an event was received and we do not know what it is");
    break;
  }
}

function
GCM_Success(e)
{
  $("#app-status-ul").append('<li>GCM_Success -> We have successfully registered and called the GCM plugin, waiting for GCM_Event:registered -> REGID back from Google</li>');

}

function
GCM_Fail(e)
{
  $("#app-status-ul").append('<li>GCM_Fail -> GCM plugin failed to register</li>');

  $("#app-status-ul").append('<li>GCM_Fail -> ' + e.msg + '</li>');

}

 /** 
 * Flags to denote the Android Notification constants
 * Values are representation from Android Notification Flag bit vals 
 */
 
function Flag() {}
Flag.FLAG_AUTO_CANCEL="16";
Flag.FLAG_NO_CLEAR="32";

/** @deprecated Use the W3C standard window.Notification API instead. */
var NotificationMessenger = function() { }

/**
 * @param title Title of the notification
 * @param body Body of the notification
 * @deprecated Use the W3C standard window.Notification API instead.
 */
NotificationMessenger.prototype.notify = function(title, body, flag) {
 
    if (window.Notification) {
        this.activeNotification = new window.Notification(title, {
            body: body,
            flag: flag
        });
    }
}

/**
 * Clears the Notificaiton Bar
 * @deprecated Use the W3C standard window.Notification API instead.
 */
NotificationMessenger.prototype.clear = function() {
    if (this.activeNotification) {
     
        this.activeNotification.close();
        this.activeNotification = undefined;
    }
}

if (!window.plugins) window.plugins = {}
if (!window.plugins.statusBarNotification) window.plugins.statusBarNotification = new NotificationMessenger();


/*
 * The W3C standard API, window.Notification. See http://www.w3.org/TR/notifications/
 * This API should be used for new applications instead of the old plugin API above.
 */
if (typeof window.Notification == 'undefined') {

    /**
     * Creates and shows a new notification.
     * @param title
     * @param options
     */
    window.Notification = function(title, options) {
        options = options || {};
        this.tag = options.tag || 'defaultTag';

        // Add this notification to the global index by tag.
        window.Notification.active[this.tag] = this;

        // May be undefined.
        this.onclick = options.onclick;
        this.onerror = options.onerror;
        this.onshow = options.onshow;
        this.onclose = options.onclose;

        var content = options.body || '';
        
        var flag = options.flag || '';

        cordova.exec(function() {
            if (this.onshow) {
                this.onshow();
            }
        }, function(error) {
            if (this.onerror) {
                this.onerror(error);
            }
        }, 'StatusBarNotification', 'notify', [this.tag, title, content, flag]);
    };

    // Permission is always granted on Android.
    window.Notification.permission = 'granted';

    window.Notification.requestPermission = function(callback) {
        callback('granted');
    };

    // Not part of the W3C API. Used by the native side to call onclick handlers.
    window.Notification.callOnclickByTag = function(tag) {
        console.log('callOnclickByTag');
        var notification = window.Notification.active[tag];
        if (notification && notification.onclick && typeof notification.onclick == 'function') {
            console.log('inside if');
            notification.onclick();
        }
    };

    // A global map of notifications by tag, so their onclick callbacks can be called.
    window.Notification.active = {};


    /**
     * Cancels a notification that has already been created and shown to the user.
     */
    window.Notification.prototype.close = function() {
        cordova.exec(function() {
            if (this.onclose) {
                this.onclose();
            }
        }, function(error) {
            if (this.onerror) {
                this.onerror(error);
            }
        }, 'StatusBarNotification', 'clear', [this.tag]);
    };
}


//***********************************Database operations code******************************************************************
	
	//create table and insert some record
    function populateDB(tx) {
    if(status == 0){  // if to avoid insertion of same record repeatedly 
    
    	 
        tx.executeSql('CREATE TABLE IF NOT EXISTS Artifacts (ARTI_TITLE TEXT,ARTI_DATE TEXT, ARTI_DESCRIP TEXT, IMG_NAME TEXT,FRM_TIME TIME, TO_TIME TIME)');
       
        tx.executeSql("INSERT INTO Artifacts(ARTI_TITLE,ARTI_DATE,ARTI_DESCRIP,IMG_NAME,FRM_TIME,TO_TIME) VALUES (?,?,?,?,?,?)",  [title,date,description,img_name,frmtime,totime]);
         
         window.location.href="home.html";
         // alert("Saved successfully");
          status = status+1;
          }
          
               
    }
 
    //function will be called when an error occurred
    function errorDB(err) {
       // alert("Error processing SQL: "+err.code);
    }
 
    //function will be called when process succeed
    function successDB() {
       // alert("successfully inserted!");
        //db.transaction(queryDB,errorCB);
    }

 
 
 function backKeyDown() {
        navigator.app.exitApp(); // To exit the app!
        //navigator.app.backHistory();
    }
    
    
    
  /**************************************Save image to SD card*************************/
  
 window.appRootDirName = "download_test";

function download_save_attamnt(url,img_name) {
	console.log("file system is ready");
	
	//alert("attachment_url : " +img_name);
	
	//Get local file system
	window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, gotFS, fail);
	var fileTransfer = new FileTransfer();
	var filePath = "/sdcard/BTMCHURCH/"+img_name;
	
	//alert("filePath : " +filePath);
	
	//alert("Saving");

	fileTransfer.download(
	    url,
	    filePath,
	    function(entry) {
	        //alert("download complete: ");
	    },
	    function(error) {
	        alert("download error" + error.source);
	    }
	);

}

/****************************Download Pdf************************************/
function download_save_pdf(url,pdfName) {
	console.log("file system is ready");
	
	//alert("attachment_url : " +pdfName);
	
	//Get local file system
	window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, gotFS, fail);
	var fileTransfer = new FileTransfer();
	var filePath = "/sdcard/BTMCHURCH/"+pdfName;
	
	//alert("filePath : " +filePath);
	
	//alert("Saving");

	fileTransfer.download(
	    url,
	    filePath,
	    function(entry) {
	        //alert("download complete: ");
	    },
	    function(error) {
	        alert("download error" + error.source);
	    }
	);

}






function fail() {
	console.log("failed to get filesystem");
}

function gotFS(fileSystem) {
	console.log("filesystem got");
	window.fileSystem = fileSystem;
	fileSystem.root.getDirectory(window.appRootDirName, {
		create : true,
		exclusive : false
	}, dirReady, fail);
}



function dirReady(entry) {
	window.appRootDir = entry;
	console.log("application dir is ready");
}  

/**************************Save Device Id****************************************/

function insertID(tx){
      
      	alert("sdaf" +device_id);
        tx.executeSql('CREATE TABLE IF NOT EXISTS DeviceID (DEV_ID TEXT)');
       
        tx.executeSql("INSERT INTO DeviceID(DEV_ID) VALUES (?)",  [device_id]);
             
    }
    
    
 