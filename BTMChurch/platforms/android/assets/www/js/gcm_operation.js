var ATTACHMENT_URL = "http://ec2-35-163-128-85.us-west-2.compute.amazonaws.com:8080/BTMChurch/data/"

// Variables used for database operations  
var device_id;
var status = 0;
var title;
var date;
var thought;
var attachment_url;
var img_name;
var flag = "false";
var imgpath;
var pdfpath;
var frmtime;
var totime;
var infid;
var pushNotification;

function onDeviceReady() {
  try {
    pushNotification = window.plugins.pushNotification;
    if (device.platform == 'android' || device.platform == 'Android' ||
      device.platform == 'amazon-fireos') {
      pushNotification.register(successHandler, errorHandler, {
        "senderID": "367160875139",
        "ecb": "onNotification"
      });
    } else {
      pushNotification.register(tokenHandler, errorHandler, {
        "badge": "true",
        "sound": "true",
        "alert": "true",
        "ecb": "onNotificationAPN"
      }); // required!
    }
  } catch (err) {
    txt = "There was an error on this page.\n\n";
    txt += "Error description: " + err.message + "\n\n";
  }
}

// handle APNS notifications for iOS
function onNotificationAPN(e) {
  if (e.alert) {
    $("#app-status-ul").append('<li>push-notification: ' + e.alert + '</li>');
    navigator.notification.alert(e.alert); // showing an alert also requires the org.apache.cordova.dialogs plugin
  }

  /*if (e.sound) {
      var snd = new Media(e.sound);  // playing a sound also requires the org.apache.cordova.media plugin
      snd.play();
  }*/

  if (e.badge) {
    pushNotification.setApplicationIconBadgeNumber(successHandler, e.badge);
  }
}


// handle GCM notifications for Android
function onNotification(e) {
  // $("#app-status-ul").append('<li>EVENT -> RECEIVED:' + e.event + '</li>');
  console.log("GCM---->"+JSON.stringify(e));
  switch (e.event) {
    case 'registered':
      if (e.regid.length > 0) {
        device_id = e.regid;
        window.localStorage.setItem("mySum", device_id);

        //Save device id in local database
        saveDevID();
      }

      break;

    case 'message':

      // if this flag is set, this notification happened while we were in the foreground.
      // you might want to play a sound to get the user's attention, throw up a dialog, etc.
      //Extracting data to store into local database
      setTimeout(function() {

        title = e.payload.message.infType;
        date = e.payload.message.infDate;
        description = e.payload.message.description;
        img_name = e.payload.message.imgPath;
        pdfpath = e.payload.message.pdfPath;
        frmtime = e.payload.message.frmTime;
        totime = e.payload.message.toTime;

        if (title == "PastorRequest") {

          infid = "p" + e.payload.message.infId;

        } else {
          infid = e.payload.message.infId;
          //alert(infid);
        }
      }, 1000);
      if (e.foreground) {
        getRecordID(infid, date);
      } else { // otherwise we were launched because the user touched a notification in the notification tray.
        if (e.coldstart) {
          //alert('killed');
          getRecordID(infid, date);
        } else {
          window.location = "home.html";
          //getRecordID(infid,date);
        }
      }

      $("#app-status-ul").append('<li>MESSAGE -> MSG: ' + e.payload.message + '</li>');

      //android only
      $("#app-status-ul").append('<li>MESSAGE -> MSGCNT: ' + e.payload.msgcnt + '</li>');

      //amazon-fireos only
      $("#app-status-ul").append('<li>MESSAGE -> TIMESTAMP: ' + e.payload.timeStamp + '</li>');
      break;

    case 'error':
      $("#app-status-ul").append('<li>ERROR -> MSG:' + e.msg + '</li>');
      alert("ERROR -> MSG: " + e.msg);
      break;

    default:

      $("#app-status-ul").append('<li>EVENT -> Unknown, an event was received and we do not know what it is</li>');
      break;
  }
}

function tokenHandler(result) {
  $("#app-status-ul").append('<li>token: ' + result + '</li>');
  // Your iOS push server needs to know the token before it can push to this device
  // here is where you might want to send it the token for later use.
}

function successHandler(result) {

  $("#app-status-ul").append('<li>success:' + result + '</li>');
}

function errorHandler(error) {

  alert(error);

  $("#app-status-ul").append('<li>error:' + error + '</li>');
}

document.addEventListener('deviceready', onDeviceReady, true);



//***********************************INSERT INTO DATABASE******************************************************************

//create table and insert some record
function populateDB1(tx) {
  //alert("Populate DB1");
  var artidate = localStorage.getItem("artidate");
  if (status == 0) { // if to avoid insertion of same record repeatedly 
    tx.executeSql('CREATE TABLE IF NOT EXISTS Artifacts (INF_ID INTEGER,ARTI_TITLE TEXT,ARTI_DATE TEXT, ARTI_DESCRIP TEXT, IMG_NAME TEXT,PDF_NAME TEXT,FRM_TIME TIME, TO_TIME TIME)');

    tx.executeSql("INSERT INTO Artifacts(INF_ID,ARTI_TITLE,ARTI_DATE,ARTI_DESCRIP,IMG_NAME,PDF_NAME,FRM_TIME,TO_TIME) VALUES (?,?,?,?,?,?,?,?)", [infid, title, artidate, description, img_name, pdfpath, frmtime, totime]);

    setTimeout(function() {
      //window.location.href="home.html";
      if (title === 'Bulletin' || title === "Thought for the day") {

        if ($("#thanksPopup-popup").hasClass("ui-popup-active")) {

          var thanksValue = document.getElementById('thanks_resp_box').value;
          localStorage.setItem("thanksValue", thanksValue);

        }
        if ($("#prayerPopup-popup").hasClass("ui-popup-active")) {

          var prayerValue = document.getElementById('prayer_resp_box').value;
          localStorage.setItem("prayerValue", prayerValue);

        }

        window.location.href = "home.html";
      }

      if (title === 'Evangelism' || title === 'Sermon' || title === "BooksAndStudy") {
        if ($("#thanksPopup-popup").hasClass("ui-popup-active")) {

          var thanksValue = document.getElementById('thanks_resp_box').value;
          localStorage.setItem("thanksValue", thanksValue);

        }
        if ($("#prayerPopup-popup").hasClass("ui-popup-active")) {

          var prayerValue = document.getElementById('prayer_resp_box').value;
          localStorage.setItem("prayerValue", prayerValue);

        }

        window.location.href = "resources.html";

      }
      if (title === 'PastorRequest' || title === 'ThanksGiving' || title === 'PrayerRequest') {

        if ($("#thanksPopup-popup").hasClass("ui-popup-active")) {

          var thanksValue = document.getElementById('thanks_resp_box').value;
          localStorage.setItem("thanksValue", thanksValue);

        }
        if ($("#prayerPopup-popup").hasClass("ui-popup-active")) {

          var prayerValue = document.getElementById('prayer_resp_box').value;
          localStorage.setItem("prayerValue", prayerValue);

        }

        window.location.href = "prayers.html";

      }
      if (title === "Event") {
        window.location.href = "events.html";
      }
    }, 5000);

    // alert("Saved successfully");
    status = status + 1;
  }



}

//function will be called when an error occurred
function errorDB(err) {
  // alert("Error processing SQL: "+err.code);
}

//function will be called when process succeed
function successDB() {
  //alert("successfully inserted!");
  //db.transaction(queryDB,errorCB);
}


/**************************************Save image to SD card*************************/

window.appRootDirName = "BTMChurch";

function download_save_attamntFromGcm(url, img_name) {

  console.log("Downloading image >>>>>>>>>>>>>>>>>>>>>>>>>> : ");
  //Get local file system

  window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, gotFS, fail);
  //var fileTransfer = new FileTransfer();
  var filePath = "/sdcard/BTMCHURCH/";

  var complete = function() {

  };

  var error = function(err) {
    alert('Error: ' + err);
  };
  var progress = function(progress) {
    //lblProgress.innerHTML = (100 * progress.bytesReceived / progress.totalBytesToReceive) + '%';
  };

  window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function(fileSystem) {

    fileSystem.root.getFile('BTMChurch/' + img_name, {
      create: true
    }, function(newFile) {

      try {

        FileDownloaderGCMFn(newFile, url,img_name); // Start the download and persist the promise to be able to cancel the download.

      } catch (err) {
        console.log('Error: ' + err);
      }

    });

  });

}

/****************************Download Pdf************************************/
function download_save_pdfFromGcm(url, pdfName, infid) {

  console.log("file system is ready");

  var filePath = "/sdcard/BTMCHURCH/";
  var complete = function() {
    //alert("complete download");
  };
  var error = function(err) {
    alert('Error: ' + err);
  };
  var progress = function(progress) {
    //lblProgress.innerHTML = (100 * progress.bytesReceived / progress.totalBytesToReceive) + '%';
  };

  window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function(fileSystem) {
    fileSystem.root.getFile('BTMChurch/' + pdfName, {
      create: true
    }, function(newFile) {
      try {
        FileDownloaderGCMFn(newFile, url,pdfName); // Start the download and persist the promise to be able to cancel the download.
      } catch (err) {
        console.log('Error: ' + err);
      }
    });
  });
}


function fail() {
  console.log("failed to get filesystem");
}

function gotFS(fileSystem) {
  //alert("filesystem got");
  window.fileSystem = fileSystem;
  fileSystem.root.getDirectory(window.appRootDirName, {
    create: true,
    exclusive: false
  }, dirReady, fail);
}

function dirReady(entry) {
  window.appRootDir = entry;
}
window.appRootDirName = "BTMChurch";
var arrFiles = [];
var url;
var name;
//TAG: Added id of artifacts 
function saveData(infid, title, d, description, img_name, pdfpath, frmtime, totime) {
  //alert('in save data');
  if (title === 'Sermon') {
    //alert('in sermon');
    var path = ATTACHMENT_URL.concat(img_name);
    //  download_save_attamntFromGcm(path,img_name);    //Save to SD Card 

    arrFiles.push(pdfpath);
    arrFiles.push(img_name);

    for (var i = 0; i < arrFiles.length; i++) {

      url = ATTACHMENT_URL.concat(arrFiles[i]);

      name = arrFiles[i];
      window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, gotFS, fail);
      var filePath = "/sdcard/BTMCHURCH/";
      var complete = function() {};
      var error = function(err) {
        alert('Error: ' + err);
      };
      var progress = function(progress) {
        //lblProgress.innerHTML = (100 * progress.bytesReceived / progress.totalBytesToReceive) + '%';
      };
      window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function(fileSystem) {
        fileSystem.root.getFile('BTMChurch/' + name, {
          create: true
        }, function(newFile) {
          try {
            FileDownloaderGCMFn(newFile, url,name); // Start the download and persist the promise to be able to cancel the download.
          } catch (err) {
            console.log('Error: ' + err);
          }
        });
      });
    }
  }

  if (title === 'Evangelism' || title === "BooksAndStudy") {
    var path = ATTACHMENT_URL.concat(pdfpath);
    setTimeout(function() {
      download_save_pdfFromGcm(path, pdfpath, infid);
    }, 1000);
  }
  if (title === 'PastorRequest' || title === 'ThanksGiving' || title === 'PrayerRequest') {
    //alert(title);
  }


  //Set date to local storage to save into DB later 
  localStorage.setItem("artidate", d);

  //Open database in exists otherwise create new
  var db = window.openDatabase("DEVICEDATA", "1", "Device data", 200000); //will create database DEVICEDATA  
  db.transaction(populateDB1, errorDB, successDB);
}

function saveDevID() {

  //Open database in exists otherwise create new
  var db = window.openDatabase("DEVICEDATA", "1", "Device data", 200000); //will create database DEVICEDATA  
  db.transaction(insertID, errorDB, successDB);

}

//create table and insert some record
function insertID(tx) {
  tx.executeSql('CREATE TABLE IF NOT EXISTS DeviceID (DEV_ID TEXT)');
  tx.executeSql("INSERT INTO DeviceID(DEV_ID) VALUES (?)", [device_id]);
  //alert("Inserted successfully");
}


/***************************************************Fetch data from database to check duplicate records******************************************/
//Get records from local database
function getRecordID(infid, date) {
  var db = window.openDatabase("DEVICEDATA", "1", "Device data", 200000); //will create database DEVICEDATA  
  db.transaction(function(tx) {
    SelectRecId(tx, infid, date)
  }, resError, Success);

}

//Select data from local DB
function SelectRecId(tx, infid, date) {
  localStorage.setItem("date", date);

  tx.executeSql("select * from Artifacts WHERE INF_ID=? ", [infid], Success, resError);

}

//function will be called when query succeed
function Success(tx, response) {
  var d = localStorage.getItem("date");
  try{
  if (response.rows.length == 0) {
    // alert("Calling save data");
    //Save above data into local DB
    saveData(infid, title, d, description, img_name, pdfpath, frmtime, totime);
  } else {
    //alert("Do nothing");
  }
  }catch(e){
    console.error("response.rows is undefined..");
  }
}

//function will be called when an error occurred
function resError(error) {

  console.log("Error processing SQL: " + error);
}
/*****************************************************************Close fetch data**********************************************************/
var downloadedFileArray=[];
function FileDownloaderGCMFn(fileEntry, url,nameOfFile) { 
 if(downloadedFileArray.indexOf(nameOfFile)<0){
  downloadedFileArray.push(nameOfFile);
  var fileTransfer = new FileTransfer();
  var fileURL = fileEntry.toURL();
  // console.log(fileURL );
  fileTransfer.download(
    url,
    fileURL,
    function(entry) {
      console.log("download complete: " + entry.toURL());
      //window.plugins.fileOpener.open('file:///sdcard/BTMChurch/cordova_bot.png');
      $.mobile.loading('hide');
      window.plugins.fileOpener.open(cordova.file.externalRootDirectory + 'BTMChurch/' + nameOfFile);

    },
    function(error) {
      console.log("download error source " + error.source);
      console.log("download error target " + error.target);
      console.log("upload error code" + error.code);
    },
    null, // or, pass false
    {
      //headers: {
      //    "Authorization": "Basic dGVzdHVzZXJuYW1lOnRlc3RwYXNzd29yZA=="
      //}
    }
  );
  }
}