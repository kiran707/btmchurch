/***********************************
Author: Mr. Anurag Dhiman 
Date: 24 Feb 2015
Copyright: @Teknika
************************************/
//Variable used to save the data for the first time into local database
var ATTACHMENT_URL = "http://ec2-35-163-128-85.us-west-2.compute.amazonaws.com:8080/BTMChurch/data/"			  
var artifact_title;
var artifact_date;
var artifact_description;
var dbinsert;

/*
Getting all the records from local database and then populate each 
section based upon title. E.g. Thought, Evangelism etc. Variable
below are used for the same purpose
*/
     var i;  
     var date; 
     var thout_title;
     var img;
	 var frmTime;
	 var toTime;
	 var desc;
	 var infid;
	
   //Get records from local database
    function getRecords() {
     	 var db = window.openDatabase("DEVICEDATA", "1", "Device data", 200000); //will create database DEVICEDATA  
		 db.transaction(SelectData, resultError, resultSuccess);      
    }
    
    //Select data from local DB
    function SelectData(tx){
    	tx.executeSql("select * from Artifacts ORDER BY date(ARTI_DATE) DESC, FRM_TIME DESC", [], resultSuccess, resultError);
    }

    //function will be called when an error occurred
    function resultError(error) {
        alert("Error processing data");
        console.log("Error processing SQL: "+error);
    }

    //function will be called when query succeed
    function resultSuccess(tx,response) {
		var watchingdata=response;
		if(response!=undefined){
       if(response.rows.length!=0){     // the following if condition check the whether the local database contain any records or not.
      		  for(i=0;i<response.rows.length;i++){ 
        			 infid = response.rows.item(i).INF_ID;
		          	 date = response.rows.item(i).ARTI_DATE;
		          	 thout_title = response.rows.item(i).ARTI_TITLE
		          	 desc = ((response.rows.item(i).ARTI_DESCRIP).substr(0,15))+'....'
		          	 img=response.rows.item(i).ARTI_IMG
		          	 var imgName = response.rows.item(i).IMG_NAME
		     		 var full_desc = response.rows.item(i).ARTI_DESCRIP
		          	 
		     	if ( thout_title === 'Thought for the day') { 	// Creating thought contents div
			 		 var margin = 10;		//Creating dynamic sections	
			     	 $("#routeContainer").append("<div style = 'margin-left: " + margin + "px; '   class = 'test' id='d"+i+"' ><p style='text-align: justify;'>"+full_desc+" <br/><b>Date:</b> <span id='t"+i+"' class='sp'><b>"+date+"</b></span><span id='t"+i+"' class='sp' style='visibility:hidden'>$"+infid+"</span> </p></div>");
       				 $("#routeContainer").trigger('refresh');  // Auto refresh div
       	  			}
      			 else if ( thout_title === 'Bulletin') { // Creating Bulletin contents div
      				 var margin = 10;			
					 $("#bulletinContainer").append('</br>'+'<li>'+full_desc+'</li>');
					 $('#d'+i).css('background', 'url(/sdcard/BTMCHURCH/'+imgName+') no-repeat').height(100).width(100);  					
      	 			}	
      			else if ( thout_title === 'Evangelism' ) { 	// Creating Evangelism contents div
      				 var margin = 10;			
     				 $("#evangelismContainer").append("<div style = 'margin-left: " + margin + "px;'   class = 'evanglsm' id='e"+i+"'  ><p style='padding-top: 90px;'><b>"+desc+"</b> <br/><span id='t"+i+"' class='sp' style='visibility:hidden'>"+date+"</span><span id='t"+i+"' class='sp' style='visibility:hidden'>$"+infid+"</span> </p></div> ");
					 $('#e'+i).css('background', 'url(img/thumbs/evan3.jpg) no-repeat').height(100).width(100);  					
       			     $("#evangelismContainer").trigger('refresh');
      			 }
      	        else if ( thout_title === 'Sermon' ) {  // Creating Sermon contents div
      	        	imgName.trim();
      	        	var imageSrc = "/sdcard/BTMChurch/"+imgName;
      	        	 var margin = 10;	
      	 			$("#sermonsContainer").append("<div style = 'margin-left: " + margin + "px;'   class = 'sermn' id='d"+i+"'  ><p style='padding-top: 90px;'><b>"+desc+"</b> <br/>Date: <span id='t"+i+"' class='sp'>"+date+"</span><span id='t"+i+"' class='sp' style='visibility:hidden'>$"+infid+"</span> </p></div> ");					
      	 			
      	 			$.ajax({
					    url: imageSrc,
					    async: false,
					    type: "HEAD",
					    success: function() {
      	        		$('#d'+i).css('background', "url(" + imageSrc + ") no-repeat").height(100).width(100);  					
      	        			 $("#sermonsContainer").trigger('refresh');
      	        		},
					    error: function () {
      	        			 $('#d'+i).css('background', 'url(img/thumbs/sermons.jpg) no-repeat').height(100).width(100);  					
              	 			 $("#sermonsContainer").trigger('refresh');
      	        		}
					});
      	        	
      	 			 /*var margin = 10;	
      	 			$("#sermonsContainer").append("<div style = 'margin-left: " + margin + "px;'   class = 'sermn' id='d"+i+"'  ><p style='padding-top: 90px;'><b>"+desc+"</b> <br/>Date: <span id='t"+i+"' class='sp'>"+date+"</span><span id='t"+i+"' class='sp' style='visibility:hidden'>$"+infid+"</span> </p></div> ");
      	 			 if(imgName == "sermons.jpg"){
        				 $('#d'+i).css('background', 'url(img/thumbs/sermons.jpg) no-repeat').height(100).width(100);  					
          	 			 $("#sermonsContainer").trigger('refresh');
      	 			 }
      	 			 else{
     				
     				 $('#d'+i).css('background', 'url(/sdcard/BTMChurch/'+imgName+') no-repeat').height(100).width(100);  					
       	 			 $("#sermonsContainer").trigger('refresh');
      	 			 }*/
      			 }
      		 else if ( thout_title === 'BooksAndStudy' ) {   // Creating Books & Study contents div
      				 var margin = 10;			
     				 $("#studyContainer").append("<div style = 'margin-left: " + margin + "px;'   class = 'study' id='d"+i+"'  ><p style='padding-top: 90px;'><b>"+desc+"</b> <br/> <span id='t"+i+"' class='sp' style='visibility:hidden'>"+date+"</span><span id='t"+i+"' class='sp' style='visibility:hidden'>$"+infid+"</span> </p></div> ");
					 $('#d'+i).css('background', 'url(img/thumbs/books.jpg) no-repeat').height(100).width(100);  					
       	 			 $("#studyContainer").trigger('refresh');
      			 }
			//Creating Pastor's requests
      		 else if ( thout_title === 'PastorRequest' ) { 
						//alert('PastorRequest');
	      	 			var margin = 10;			
	     	 			$("#pastorreqContainer").append("<div style = 'margin-left: " + margin + "px;'   class = 'pastor_req pastorRequest' id='p"+i+"'  ><p style='padding-top: 90px;'><b>"+desc+"</b> <br/>Date: <span id='t"+i+"' class='sp'>"+date+"</span><span id='t"+i+"' class='sp' style='visibility:hidden'>$"+infid+"</span> </p></div> ");
				        $('#d'+i).css('background', 'url(img/thumbs/prayer.jpg) no-repeat').height(100).width(100);  					
	       			    $("#pastorreqContainer").trigger('refresh');
      	       }
			
			 //Creating Request contents div   /**** Tag- modified by Neeraj ******/
      	 		else if ( thout_title === 'PrayerRequest') { 
      				 var margin = 10;			
					 $("#prayerContainer").append('</br>'+'<li>'+full_desc+'</li>');
      	 		}
			//Creating thanksgiving content div
      		 else if ( thout_title === 'ThanksGiving' ) { 
	      	 		var margin = 10;			
					$("#thanksgivingContainer").append('</br>'+'<li>'+full_desc+'</li>');
      		 }
			
			//Creating search Result content div
      		 else if ( thout_title === 'SearchResult') {
      				 var margin = 10;			
      				 $("#searchContainer").append('</br>'+'<li>'+full_desc+'</li>'); //Append contents and setting back image	
					 $('#d'+i).css('background', 'url(/sdcard/BTMChurch/'+imgName+') no-repeat').height(100).width(100);  		 		
      	 	}
		
	//Onclick each evangelism do stuff
	$('#e'+i).click(function(){   
        var eveng_data = $(this).text();//Get div data
        var eveng_date =eveng_data.substring(eveng_data.lastIndexOf('$'));   //Get Date from div contents
        eveng_date = eveng_date.replace('$', '').trim();   //Remove the colon form date
        
		popUp(eveng_date); 
	});		

    $('#d'+i).click(function () {
        var div_data = $(this).text(); //Get div data
        var arti_date =div_data.substring(div_data.lastIndexOf('$'));     //Get Date from div contents
      	artiId = arti_date.replace('$', '').trim();
   
        popUp(artiId);    
  		 });
          
      } //For loop closed
	  
	   $(document).on('click',".pastorRequest",function(){
			//alert("pastor req");
			var div_data = $(this).text();
			var arti_date =div_data.substring(div_data.lastIndexOf('$'));
			artiId = arti_date.replace('$', '').trim();
			 popUp(artiId); 
		});
		
		 $(document).on('click',".thanksGiving",function(){
			//alert("thanks req");
			var div_data = $(this).text();
			var arti_date =div_data.substring(div_data.lastIndexOf('$'));
			artiId = arti_date.replace('$', '').trim();
			 popUp(artiId); 
		});
	  
		$(document).on('click',".prayerRequest",function(){
			//alert("prayer req");
			var div_data = $(this).text();
			var arti_date =div_data.substring(div_data.lastIndexOf('$'));
			artiId = arti_date.replace('$', '').trim();
			 popUp(artiId); 
		});
	  
      }  
	}
	  
 } //function closed
    
   
/****************************************Get contents for popup*****************************************************************/

function popUp(artiId) { 
	 var db = window.openDatabase("DEVICEDATA", "1", "Device data", 200000); //will create database DEVICEDATA  
	     db.transaction( function(tx){ SelectDesc(tx, artiId ) }, resultError,resultSuccess );
	}
	function SelectDesc(tx,artiId){
    	tx.executeSql('SELECT * FROM Artifacts WHERE INF_ID=?', [artiId],resultDesc, resultError);
    }
 
   var title;
   var des;
   var pdfname;
   var dt;
   
   function resultDesc(tx,response){
		for(var i=0;i<response.rows.length;i++){ 
          title = response.rows.item(i).ARTI_TITLE;
          dt = response.rows.item(i).ARTI_DATE;
          des = response.rows.item(i).ARTI_DESCRIP;
		  infId = response.rows.item(i).INF_ID;
          pdfname = response.rows.item(i).PDF_NAME;
          }
          
        if(title === 'Thought for the day' || title=== 'Bulletin') {  
         if(title === 'Thought for the day')  //TAG: To make title as Thought only
          			title = 'Thought';
         		//Call to change date format
        		var formatedDate = formatDate(dt);
         		 $("#thougtTitle").text(''+title);  //TAG: Chaged to add date with thought title
	 	  		 $("#thougtDiv").text(''+des);
				
	 	   		//Popup the thought
	 	   		$("#popupDialog").popup('open');
	 	   }
		   
		   if(title === 'PastorRequest'){
			   
			   var formatedDate = formatDate(dt);
			   title = 'PastorRequest';
			   $("#thougtTitleForPastor").text(''+title);
			   $("#thougtDivForPastor").text(''+des);
			   $("#infidStore").text(infId);
			   $("#popupDialogForPastor").popup('open');
		   }
		   
		   if(title === 'ThanksGiving' || title === 'PrayerRequest'){
			   //alert(title);
			   if(title === 'ThanksGiving'){
			   title = 'ThanksGiving';
			   //alert(title);
				}	
				if(title === 'PrayerRequest'){
			   title = 'PrayerRequest';
			   //alert(title);
				}	
         		//Call to change date format
        		var formatedDate = formatDate(dt);
				 //$("#thougtTitle").text('');
         		 $("#thougtTitle1").text(''+title);  //TAG: Chaged to add date with thought title
				// alert(des);
	 	  		 $("#thougtDivPrayersPage").text(''+des);
				
	 	   		//Popup the thought
	 	   		$("#popupDialog1").popup('open');
		   }
	 	    if(title === 'Evangelism' || title=== 'Sermon' || title=== 'BooksAndStudy') {
	 	    //TAG: Chnaged code for opening pdf file with cordova file opener plugins V1.0
		    //window.plugins.fileOpener.open('file:///sdcard/BTMChurch/'+pdfname);
		    var attachmentSrc = 'file:///sdcard/BTMChurch/'+pdfname;
		    $.ajax({
			    url: attachmentSrc,
			    async: false,
			    type: "HEAD",
			    success: function() {
		    	window.plugins.fileOpener.open('file:///sdcard/BTMChurch/'+pdfname);		    	 
	        		},
			    error: function () {
					    $.mobile.loading('show');
	        			downloadIfNotExists(pdfname);
	        		}
			});
	 	   
	 	    }
    }
/***************************************************Change date Format**************************************************************/
function formatDate (input) {
  				var datePart = input.match(/\d+/g),
  				year = datePart[0].substring(2), // get only two digits
 				 month = datePart[1], day = datePart[2];
 				 return day+'-'+month+'-'+year;
				}
function displayDate() {
            var currentDate = new Date();
		    var day = currentDate.getDate();
		    var month =currentDate.getMonth() + 1;
		    var year = currentDate.getFullYear();
			var date= year+ "-" + month + "-" + day ;
            return date;
         }
 /*****************************************************Getting all events for calendar display******************************************/
 function getEvent(){
 		 var db = window.openDatabase("DEVICEDATA", "1", "Device data", 200000); //will create database DEVICEDATA  
		 db.transaction(SelectEvent, resultError, eventResult);  
		 }
 
 function SelectEvent(tx){
    	tx.executeSql("select * from Artifacts", [], eventResult, resultError);
    }
    
 function eventResult(tx,response) {
 	 var date;
 	 var data=[];
	 for(i=0;i<response.rows.length;i++){       	 
            var title = response.rows.item(i).ARTI_TITLE
          	img=response.rows.item(i).ARTI_IMG
         	if(title=='Event'){
          	 	 date = response.rows.item(i).ARTI_DATE;
          	 	 frmTime = response.rows.item(i).FRM_TIME
          	 	 toTime = response.rows.item(i).TO_TIME
          	 	 desc= response.rows.item(i).ARTI_DESCRIP
          	 	 data.push(date);
          	 	 data.push(frmTime+' '+toTime+' '+desc); 
          	 }
          }
          window.localStorage.setItem("evt", data);
	}



/********************************************************first time registaion get top records*****************************************/
function insertRecords(obj){
  		dbinsert = window.openDatabase("DEVICEDATA", "1", "Device data", 200000); //will create database DEVICEDATA  
		dbinsert.transaction(function (tx) {
 
 			for (var i = 0; i < obj.length; i++) {
     			var pdf_name;
     			var infid;
    			var artifact_title = obj[i].infType;
    			if(artifact_title == "PastorRequest"){
   	 									infid = "p"+obj[i].infId;
   	 									}
										else{
											infid = obj[i].infId;
										}
    			   /************** changing here****************/
    			  
				var artifact_date 	= obj[i].infDate;
				
	 			if(artifact_date == ""){ artifact_date = i; } //Date is not mandetory for all
						var artifact_description = obj[i].description;
						var frm_time=obj[i].frmTime;
						var to_time=obj[i].toTime;
						pdf_name = obj[i].pdfPath;
						
				if(artifact_title === 'Sermon')
					 {
			 			var img_name = obj[i].imgPath;
			 			var path = ATTACHMENT_URL.concat(img_name);	
			 			//Save to SD Card
  						download_save_attamnt(path,img_name);	
					 }
				if(artifact_title === 'Evangelism' || artifact_title=== 'Sermon' || artifact_title === 'BooksAndStudy')
					 {
			 			pdf_name = obj[i].pdfPath;
			 			var path = ATTACHMENT_URL.concat(pdf_name);	
							
			 			//Save to SD Card
  						download_save_pdf(path,pdf_name);	
  					 }
				
				if(artifact_title === 'PastorRequest' || artifact_title=== 'PrayerRequest' || artifact_title === 'ThanksGiving'){
					//alert(artifact_title);
				}
			 
			 tx.executeSql('CREATE TABLE IF NOT EXISTS Artifacts (INF_ID TEXT UNIQUE,ARTI_TITLE TEXT,ARTI_DATE DATE, ARTI_DESCRIP TEXT, IMG_NAME TEXT, PDF_NAME TEXT,FRM_TIME TIME,TO_TIME)');
   			 tx.executeSql("INSERT INTO Artifacts(INF_ID,ARTI_TITLE,ARTI_DATE,ARTI_DESCRIP, IMG_NAME, PDF_NAME,FRM_TIME,TO_TIME) VALUES (?,?,?,?,?,?,?,?)",  [infid,artifact_title,artifact_date,artifact_description,img_name, pdf_name,frm_time,to_time]);
	  
		}
	
	});
	
	 return "success";
}

    //function will be called when an error occurred
    function errorDB(err) {
       alert("Error processing data");
    }
 
    //function will be called when process succeed
    function successDB() {
      // alert("successfully inserted!");
        //db.transaction(queryDB,errorCB);
    }


/*************************************************************Download Images************************************************************/
function onFail(message) {
      alert('Failed because: ' + message);
    }

function gotFS(fileSystem) {
    fileSystem.root.getFile("/sdcard/BTMCHURCH/test2.jpg", {create: true}, gotFileEntry, fail);
	}

function gotFileEntry(fileEntry) {
    fileEntry.file(gotFile, fail);
	}

function gotFile(file){
    readDataUrl(file);  
}

function readDataUrl(file) {
       var reader = new FileReader();
       reader.onloadend = function(evt) {
       console.log("Read as data URL");
       console.log(evt.target.result);
       document.getElementById("smallImage").style.display='block'; 
       document.getElementById("smallImage").src = evt.target.result;   
    	}; 
    	reader.readAsDataURL(file);
	}

function fail(evt) {
    console.log(evt.target.error.code);
}



/*****************************************************************Save image to SD card**************************************************/
  
 window.appRootDirName = "BTMChurch";

function download_save_attamnt(url,img_name) {

	var filePath = "/sdcard/BTMCHURCH/";
	        var complete = function() {
        };
        var error = function (err) {
            alert('Error: ' + err);
            
        };
        var progress = function(progress) {
            //lblProgress.innerHTML = (100 * progress.bytesReceived / progress.totalBytesToReceive) + '%';
        };
	
	window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function(fileSystem) {
	fileSystem.root.getFile("/BTMChurch/"+img_name, { create: true }, function (newFile) {
		try {
           /*  var downloader = new BackgroundTransfer.BackgroundDownloader();
		   // Create a new download operation.
            var download = downloader.createDownload(url, newFile);
			
            // Start the download and persist the promise to be able to cancel the download.
            downloadPromise = download.startAsync().then(complete, error, progress);
 */        FileDownloaderFn(newFile, url, false);  //3rd parameter is false that means after download dont show the file..
     
	 } catch(err) {
            console.log('Error: ' + err);
        }
                
            });
	});

}

/******************************************************************Download Pdf********************************************************************/
function download_save_pdf(url,pdfName) {
	
	//Get local file system
	var filePath = "/sdcard/BTMCHURCH/";
	  var complete = function() {
           
        };
        var error = function (err) {
            alert('Error: ' + err);
            
        };
        var progress = function(progress) {
            //lblProgress.innerHTML = (100 * progress.bytesReceived / progress.totalBytesToReceive) + '%';
        };
		
		window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function(fileSystem) {
	   fileSystem.root.getFile('BTMChurch/'+pdfName, { create: true }, function (newFile) {
		try {
           /*  var downloader = new BackgroundTransfer.BackgroundDownloader();
		   // Create a new download operation.
            var download = downloader.createDownload(url, newFile);
            // Start the download and persist the promise to be able to cancel the download.
            downloadPromise = download.startAsync().then(complete, error, progress); */
			 FileDownloaderFn(newFile, url, false);//3rd parameter is false that means after download dont show the file..
        } catch(err) {
            console.log('Error: ' + err);
        }
                
            });
		});
}

function fail() {
	console.log("failed to get filesystem");
}

function gotFS(fileSystem) {
	window.fileSystem = fileSystem;
	fileSystem.root.getDirectory(window.appRootDirName, {
		create : true,
		exclusive : false
	}, dirReady, fail);
}

function dirReady(entry) {
	window.appRootDir = entry;
}  


/**************displaying the Thought for the day for the first time app install********************************************/

function displayThought(toDay){
 
  var db = window.openDatabase("DEVICEDATA", "1", "Device data", 200000); //will create database DEVICEDATA  
 // db.transaction(function(tx){ SelectDesc(tx,toDay) }, resultError, thoughtResult); 
  db.transaction( function(tx){ SelectThought(tx, toDay ) }, resultError,thoughtResult );
}
function SelectThought(tx,toDay){
 
 tx.executeSql("select * from Artifacts WHERE ARTI_DATE=?  ", [toDay], thoughtResult, resultError);
}
function thoughtResult(tx,response) {
  var flg=  window.localStorage.getItem("flag");// this will get the flag value from the window object.
 if(flg==0){
 	for(var i=0;i<response.rows.length;i++){ 
 
	 		var dat = response.rows.item(i).ARTI_DATE;
	        var title = response.rows.item(i).ARTI_TITLE;
	        var desc = response.rows.item(i).ARTI_DESCRIP; 
	        var formatedDate = formatDate(dat);
     if(title=='Thought for the day'){
       $("#thougtTitle").text(''+'Thought:'+formatedDate);
    $("#thougtDiv").text(''+desc);
    
    //Popup the thought
    $("#popupDialog").popup('open');
    }
         }
 }
	 flg=flg+1;// increasing the flag value
	 window.localStorage.setItem("flag", flg);//reassigning the flag value to reduce the pop again and again.
	 // changed fg to flg
	 //removed 1
}



/****************************************************PDF OPENER******************************************************************************/
function FileOpener() {
};

FileOpener.prototype.open = function(url) {
    cordova.exec(null, null, "FileOpener", "openFile", [url]);
};

/**
 * Load Plugin
 */

if(!window.plugins) {
    window.plugins = {};
}
if (!window.plugins.fileOpener) {
    window.plugins.fileOpener = new FileOpener();
}

var userDetails;
function insertUser(obj){
	 userDetails = obj;
		db = window.openDatabase("DEVICEDATA", "1", "Device data", 200000); //will create database DEVICEDATA  
		db.transaction(function(tx){
				var firstName=userDetails.fname;
				var lastName=userDetails.lname;
				var dateOfBirth=userDetails.dob;
				var gender=userDetails.ged;
				var emailId=userDetails.email;
				var mobNo=userDetails.mobile_no;
				var anniversary=userDetails.anniv;
				var deviceId=userDetails.device_id;
				
			 tx.executeSql('CREATE TABLE IF NOT EXISTS USERINFO (FIRST_NAME TEXT,LAST_NAME TEXT,DATE_OF_BIRTH DATE,GENDER TEXT,EMAIL TEXT,MOBILE_NO TEXT,ANNIVERSARY DATE,DEVICE_ID TEXT)');
			 tx.executeSql("INSERT INTO USERINFO(FIRST_NAME,LAST_NAME,DATE_OF_BIRTH,GENDER,EMAIL,MOBILE_NO,ANNIVERSARY,DEVICE_ID) VALUES (?,?,?,?,?,?,?,?)",  [firstName,lastName,dateOfBirth,gender,emailId, mobNo,anniversary,deviceId]);
		});

}
function errorCB(){
	alert("error while opening the database");
}

function successCB(){
}


/******************************************
 * Download file if not exists in folder
 **********************************************/
function downloadIfNotExists(pdfname){
	  
	// var lblProgress = document.getElementById('lblProgress');
	var url = ATTACHMENT_URL.concat(pdfname);
		 
		var name = pdfname;
		 
		window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, gotFS, fail);
		var filePath = "/sdcard/BTMCHURCH/";
		var complete = function() {
	        };
	        var error = function (err) {
	            alert('Error: ' + err);
	        };
	        var progress = function(progress) {
	            //lblProgress.innerHTML = (100 * progress.bytesReceived / progress.totalBytesToReceive) + '%';
	        	
	        
	        };
		window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function(fileSystem) {
		fileSystem.root.getFile('BTMChurch/'+name, { create: true }, function (newFile) {
			try {
	           FileDownloaderFn(newFile, url, true);//3rd parameter is true that means after downloading open the file..	      	
			} catch(err) {
	            console.log('Error: ' + err);
	            alert("Sorry! file does not exists on server.");
	        }     
	      });
		});
}

function FileDownloaderFn(fileEntry, uri, readBinaryData) {

    var fileTransfer = new FileTransfer();
    var fileURL = fileEntry.toURL();
   // console.log(fileURL ); 
   fileTransfer.download(
        uri,
        fileURL,
        function (entry) {
            console.log("download complete: " + entry.toURL());
            //window.plugins.fileOpener.open('file:///sdcard/BTMChurch/cordova_bot.png');
			if(readBinaryData){
			$.mobile.loading('hide');	
			window.plugins.fileOpener.open(cordova.file.externalRootDirectory+'BTMChurch/'+pdfname);
			}
        },
        function (error) {
            console.log("download error source " + error.source);
            console.log("download error target " + error.target);
            console.log("upload error code" + error.code);
        },
        null, // or, pass false
        {
            //headers: {
            //    "Authorization": "Basic dGVzdHVzZXJuYW1lOnRlc3RwYXNzd29yZA=="
            //}
        }
    );
}

