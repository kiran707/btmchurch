/*******************************Search Operation Code*****************************************************/
var qry;
var infid;
var date;
var thout_title;
var desc;
var img;
 var creteria_value;
 var search_creteria;
 var searchContent;
 var fromDate_value;
 var toDate_value;
 var search_value;
 var imgName;
 var full_desc;
function globalSearch() {

	//alert("Search");
	var db = window.openDatabase("DEVICEDATA", "1", "Device data", 200000); //will create database DEVICEDATA 
	//alert("Search2");
	search_creteria = document.getElementById('srch_criteria');
	creteria_value = search_creteria.options[search_creteria.selectedIndex].value;
	searchContent = document.getElementById('searchTxt').value;
	
	
	fromDate_value = document.getElementById("fromDate").value;
		//alert("fromDate_value : " +fromDate_value);
		
		toDate_value = document.getElementById("toDate").value;
		//alert("toDate_value : " +toDate_value);
		
		var fromDateValue = Date.parse(fromDate_value);
		var toDateValue = Date.parse(toDate_value);
		
	
		search_value = document.getElementById('searchTxt').value;
		//alert("search_value : " +search_value);
	//alert(creteria_value);
	//alert("search content"+searchContent);
	//db.transaction(SelectData, resultError, resultSuccess); 
	if(creteria_value == "" || creteria_value == "Select Content" ){alert("Please Select Type"); return false;}
	if(fromDateValue > toDateValue){alert("From date should be less than to date"); return false;}
	
	if(creteria_value == "All" ) {
	
		if(search_value == ""){
			//alert("I am executing without search value");
			qry="select * from Artifacts WHERE ARTI_DATE BETWEEN '"+fromDate_value+"' AND '"+toDate_value+"' ORDER BY date(ARTI_DATE) DESC,time(FRM_TIME) DESC" ;
			db.transaction(SelectData, resultError, resultSuccess); 
		}
		else if(fromDate_value == "" || toDate_value == ""){
			//alert("I am executing without dates");
			qry="select * from Artifacts WHERE ARTI_DESCRIP LIKE '%"+ searchContent+"%' ORDER BY date(ARTI_DATE) DESC,time(FRM_TIME) DESC";
			db.transaction(SelectData, resultError, resultSuccess); 
		}
		
		else{
			//alert("I am executing with all values");
		 qry="select * from Artifacts WHERE ARTI_DATE BETWEEN '"+fromDate_value+"' AND '"+toDate_value+"' AND ARTI_DESCRIP LIKE '%"+ searchContent+"%' ORDER BY date(ARTI_DATE) DESC,time(FRM_TIME) DESC";
		 db.transaction(SelectData, resultError, resultSuccess); 
		 }
		//alert("db.transaction");
	}
	else if(creteria_value == "Sermon"  || creteria_value == "Thought for the day" || creteria_value == "Bulletin" || creteria_value == "Evangelism" || creteria_value == "BooksAndStudy" || creteria_value == "PastorRequest" || creteria_value == "PrayerRequest" || creteria_value == "ThanksGiving"){ 
		
		if(search_value == ""){
			//alert("I am executing without search value");
			qry="select * from Artifacts WHERE ARTI_TITLE ='"+creteria_value+"' AND ARTI_DATE BETWEEN '"+fromDate_value+"' AND '"+toDate_value+"' ORDER BY date(ARTI_DATE) DESC,time(FRM_TIME) DESC";
			db.transaction(SelectData, resultError, resultSuccess); 
		}
		else if(fromDate_value == "" || toDate_value == ""){
			//alert("I am executing without dates");
			qry="select * from Artifacts WHERE ARTI_TITLE ='"+creteria_value+"' AND ARTI_DESCRIP LIKE '%"+ searchContent+"%' ORDER BY date(ARTI_DATE) DESC,time(FRM_TIME) DESC";
			db.transaction(SelectData, resultError, resultSuccess); 
		}
			else{
			qry = "SELECT * FROM Artifacts WHERE ARTI_TITLE ='"+creteria_value+"' AND ARTI_DATE BETWEEN '"+fromDate_value+"' AND '"+toDate_value+"' AND ARTI_DESCRIP LIKE '%"+ searchContent+"%' ORDER BY date(ARTI_DATE) DESC,time(FRM_TIME) DESC";
			db.transaction(SelectData, resultError, resultSuccess); 
			}
		}
		
		else{
			alert("Check Your Type");
		}
		
}                            //////////////////////////******** Global search ended here.
    
	function SelectData(tx){
    
    	tx.executeSql(qry, [], result1Success, result1Error);
		//alert("executeSql");
    }
    
    
    //function will be called when an error occurred
    function resultError(error) {
        //alert("Error processing SQL: "+error);
		alert("resultError" +error.code);
    }
	
	 function result1Error(error) {
		alert("ResultError" +error.code);
    }
	
	function resultSuccess() {
     
    }
 
 
 
    //function will be called when query succeed
    function result1Success(tx,response) {
   //alert(response.rows.length);
       // the following if condition check the whether the local database contain any records or not.
       if(response.rows.length!=0){
       		//alert("before loop");
     		   //iterate records
			   $(".displaySearchResult").empty();
			   $(".searchResult").hide();
      		  for(i=0;i<response.rows.length;i++){ 
        
        			 infid = response.rows.item(i).INF_ID;
		          	 date = response.rows.item(i).ARTI_DATE;
		          	 thout_title = response.rows.item(i).ARTI_TITLE;
		          	desc = ((response.rows.item(i).ARTI_DESCRIP).substr(0,10))+'..'
		          	 img=response.rows.item(i).ARTI_IMG;
		          	 imgName = response.rows.item(i).IMG_NAME;
		     		 full_desc = response.rows.item(i).ARTI_DESCRIP;
				
				displayArtifacts();
   
          
      } //For loop closed
	  //alert("after loop of success");
      }  
	  else{
		  //alert("entered into else block");
		  var json = {"searchContent":searchContent,"creteria_value":creteria_value,"fromDate_value":fromDate_value,"toDate_value":toDate_value}
		  //alert(json);
		   $.ajax({
							url : SEARCH_ARTIFACTS,

							type: 'POST',

							dataType : "json",
							contentType:'application/json',

							data:JSON.stringify(json),
							crossDomain:true,
							
							success:function(response) { 
								$(".displaySearchResult").empty();
								$(".searchResult").hide();
								//alert("success" +response);
								
								if(response[0].title == "No records for your criteria"){
									alert("No records for your criteria");
								}
								
								else{
								for(var i=0;i<=response.length;i++){
									
									//infid = response[i].;
									date = response[i].date;
									thout_title = response[i].title;
									desc = ((response.rows.item(i).ARTI_DESCRIP).substr(0,8))+'..'
									//alert(desc);
									img=response[i].imagePath;
									//imgName = response.rows.item(i).IMG_NAME;
									full_desc = response[i].description;
									fromTime = response[i].fromTime;
									toTime = response[i].toTime;
									pdfPath = response[i].pdfPath;
									//alert(response[i].title);
									//alert(response[i].description);
									
									displayArtifacts();                 ////***** displaying artifacts
								}
								
								}
								
								 
								},

							error: function(e) {
							//alert("Error" +e);
							alert('Not able to find BTM Church Server.Please call Teknika at 080-22260160.'); 
							}
						});
	  }
      
 } //function closed
 
 
	//************* ONClick Popup for thought for the day	 
$(document).on('click','.thought' , function(){ 
     // Your Code
	  //alert("hi");
	  var div_data = $(this).text();
	  var arti_date =div_data.substring(div_data.lastIndexOf('$'));
	   artiId = arti_date.replace('$', '').trim();
	    popUp(artiId);
 });
 
 $(document).on('click','.prayerAndThanks' , function(){ 
     // Your Code
	  //alert("hi");
	  var div_data = $(this).text();
	  var arti_date =div_data.substring(div_data.lastIndexOf('$'));
	   artiId = arti_date.replace('$', '').trim();
	    popUp(artiId);
 });
 
 $(document).on('click',".pastorRequest",function(){
		var div_data = $(this).text();
		var arti_date =div_data.substring(div_data.lastIndexOf('$'));
		artiId = arti_date.replace('$', '').trim();
		 popUp(artiId); 
		 
	});
 
 $(document).on("click","#pastorRequestCancel",function(){
		//alert("prayer cancel");   pastorRequestCancel
		$('#resp_box').val('');
		$("#popupDialogForPastor").popup("close");
		
	});
 
 //Onclick each evangelism do stuff
	$(document).on('click','.evanBSSermon' , function(){  
	 
	 //Get div data
        var eveng_data = $(this).text();
        //Get Date from div contents
        var eveng_date =eveng_data.substring(eveng_data.lastIndexOf('$'));
      
        //Remove the colon form date
        eveng_date = eveng_date.replace('$', '').trim();
        
		popUp(eveng_date); 
  
 });		
 
 function displayArtifacts(){
	 
	 if ( thout_title === 'Thought for the day') {
					$("#displaySearchRouteContainer").show();
 					 //Creating dynamic sections
			 		 var margin = 10;			
			     	 $("#searchRouteContainer").append("<div style = 'margin-left: " + margin + "px; '   class = 'thought srch' id='t"+i+"' ><p style='text-align: justify;'>"+full_desc+" <br/><b>Date:</b> <span id='t"+i+"' class='sp'><b>"+date+"</b></span><span id='t"+i+"' class='sp' style='visibility:hidden'>$"+infid+"</span> </p></div>");
					 
					 // Auto refresh div
       				 $("#searchRouteContainer").trigger('refresh');
       	  			}
				else if ( thout_title === 'Bulletin') {
					$("#displaySearchBulletinContainer").show();
 					 //Creating dynamic sections
			 		 var margin = 10;			
			     	 $("#searchBulletinContainer").append("<div style = 'margin-left: " + margin + "px; '   class = 'thought srch' id='t"+i+"' ><p style='text-align: justify;'>"+full_desc+" <br/><b>Date:</b> <span id='t"+i+"' class='sp'><b>"+date+"</b></span><span id='t"+i+"' class='sp' style='visibility:hidden'>$"+infid+"</span> </p></div>");
					 
					 // Auto refresh div
       				 $("#searchBulletinContainer").trigger('refresh');
       	  			}
				// Creating Evangelism contents div
      			else if ( thout_title === 'Evangelism' ) { 
					$("#displaySearchEvangelismContainer").show();
				
      				 var margin = 10;			
     				 $("#searchEvangelismContainer").append("<div style = 'margin-left: " + margin + "px;'   class = 'evanglsm evanBSSermon' id='e"+i+"'  ><p style='padding-top: 90px;'><b>"+desc+"</b> <br/><span id='t"+i+"' class='sp' style='visibility:hidden'>"+date+"</span><span id='t"+i+"' class='sp' style='visibility:hidden'>$"+infid+"</span> </p></div> ");
					 $('#e'+i).css('background', 'url(img/thumbs/evan3.jpg) no-repeat').height(100).width(100);  					
       			     $("#searchEvangelismContainer").trigger('refresh');
      			 }
				  // Creating Sermon contents div
      	        else if ( thout_title === 'Sermon' ) {
					$("#displaySearchSermonsContainer").show();
      	 			 var margin = 10;			
     				 $("#searchSermonsContainer").append("<div style = 'margin-left: " + margin + "px;'   class = 'sermn evanBSSermon' id='d"+i+"'  ><p style='padding-top: 90px;'><b>"+desc+"</b> <br/><span id='t"+i+"' class='sp'>"+date+"</span><span id='t"+i+"' class='sp' style='visibility:hidden'>$"+infid+"</span> </p></div> ");
     				 var imageSrc = "/sdcard/BTMChurch/"+imgName;
     				$.ajax({
					    url: imageSrc,
					    async: false,
					    type: "HEAD",
					    success: function() {
     					$('#d'+i).css('background', 'url('+imageSrc+') no-repeat').height(100).width(100);  					
          	 			 $("#searchSermonsContainer").trigger('refresh');
      	        		},
					    error: function () {
      	        			 $('#d'+i).css('background', 'url(img/thumbs/sermons.jpg) no-repeat').height(100).width(100);  					
              	 			 $("#sermonsContainer").trigger('refresh');
      	        		}
					});
     				 
     				 
     				 /*
     				 $('#d'+i).css('background', 'url(/sdcard/BTMChurch/'+imgName+') no-repeat').height(100).width(100);  					
       	 			 $("#searchSermonsContainer").trigger('refresh');*/
      			 }
      	 
      	  	  // Creating Books & Study contents div
      		 else if ( thout_title === 'BooksAndStudy' ) {
					$("#displaySearchStudyContainer").show();
			 
      				 var margin = 10;			
     				 $("#searchStudyContainer").append("<div style = 'margin-left: " + margin + "px;'   class = 'study evanBSSermon' id='d"+i+"'  ><p style='padding-top: 90px;'><b>"+desc+"</b> <br/> <span id='t"+i+"' class='sp' style='visibility:hidden'>"+date+"</span><span id='t"+i+"' class='sp' style='visibility:hidden'>$"+infid+"</span> </p></div> ");
					 $('#d'+i).css('background', 'url(img/thumbs/books.jpg) no-repeat').height(100).width(100);  					
       	 			 $("#searchStudyContainer").trigger('refresh');
      			 }
				 
				 //Creating Pastor's requests
      		 else if ( thout_title === 'PastorRequest' ) { 
					$("#displaySearchPastorreqContainer").show();
	      	 			var margin = 10;			
	     	 			$("#searchPastorreqContainer").append("<div style = 'margin-left: " + margin + "px;'   class = 'pastor_req pastorRequest' id='p"+i+"'  ><p style='padding-top: 90px;'><b>"+desc+"</b> <br/>Date: <span id='t"+i+"' class='sp'>"+date+"</span><span id='t"+i+"' class='sp' style='visibility:hidden'>$"+infid+"</span> </p></div> ");
				        $('#d'+i).css('background', 'url(img/thumbs/prayer.jpg) no-repeat').height(100).width(100);  					
	       			    $("#searchPastorreqContainer").trigger('refresh');
      	   
      		 }
			   
			   else if ( thout_title === 'PrayerRequest' ) { 
					$("#displaySearchPrayerContainer").show();
						//alert('PastorRequest');
	      	 			var margin = 10;			
	     	 			/*$("#searchPrayerContainer").append("<div style = 'margin-left: " + margin + "px;'   class = 'pastor_req pastorRequest prayerAndThanks' id='d"+i+"'  ><p style='padding-top: 90px;'><b>"+desc+"</b> <br/><span id='t"+i+"' class='sp'>"+date+"</span><span id='t"+i+"' class='sp' style='visibility:hidden'>$"+infid+"</span> </p></div> ");
				        $('#d'+i).css('background', 'url(img/thumbs/prayer.jpg) no-repeat').height(100).width(100);  					
	       			    $("#searchPrayerContainer").trigger('refresh');
	       			    */
	      	 		
	       			 $("#searchPrayerContainer").append('</br>'+'<li>'+full_desc+'</li>');
					  $("#searchPrayerContainer").trigger('refresh');
      	       }
			   
			   else if ( thout_title === 'ThanksGiving' ) { 
					$("#displaySearchThanksgivingContainer").show();
						//alert('PastorRequest');
	      	 			var margin = 10;			
	     	 			/*$("#searchThanksgivingContainer").append("<div style = 'margin-left: " + margin + "px;'   class = 'pastor_req pastorRequest prayerAndThanks' id='d"+i+"'  ><p style='padding-top: 90px;'><b>"+desc+"</b> <br/><span id='t"+i+"' class='sp'>"+date+"</span><span id='t"+i+"' class='sp' style='visibility:hidden'>$"+infid+"</span> </p></div> ");
				        $('#d'+i).css('background', 'url(img/thumbs/thanks.jpg) no-repeat').height(100).width(100);  					
				       */
				        $("#searchThanksgivingContainer").append('</br>'+'<li>'+full_desc+'</li>');
				        $("#searchThanksgivingContainer").trigger('refresh');
      	       }
				else{}
 }
		 
