package com.plugin.gcm;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.android.gcm.GCMBaseIntentService;

@SuppressLint("NewApi")
public class GCMIntentService extends GCMBaseIntentService {

	private static final String TAG = "GCMIntentService";
	
	String title;
	String infId;
	String description;
	
	public GCMIntentService() {
		super("GCMIntentService");
	}

	@Override
	public void onRegistered(Context context, String regId) {

		Log.v(TAG, "onRegistered: "+ regId);

		JSONObject json;

		try
		{
			json = new JSONObject().put("event", "registered");
			json.put("regid", regId);

			Log.v(TAG, "onRegistered: " + json.toString());

			// Send this JSON data to the JavaScript application above EVENT should be set to the msg type
			// In this case this is the registration ID
			PushPlugin.sendJavascript( json );

		}
		catch( JSONException e)
		{
			// No message to the user is sent, JSON failed
			Log.e(TAG, "onRegistered: JSON exception");
		}
	}

	@Override
	public void onUnregistered(Context context, String regId) {
		Log.d(TAG, "onUnregistered - regId: " + regId);
	}

	@Override
	protected void onMessage(Context context, Intent intent) {
		Log.d(TAG, "onMessage - context: " + context);

		// Extract the payload from the message
		Bundle extras = intent.getExtras();
		
		String message = intent.getExtras().getString("message");
		JSONObject jsonObject;
	    try {
	          jsonObject = new JSONObject(message);
	          title = jsonObject.getString("infType");
	          
	          infId = jsonObject.getString("infId");
	   	      System.out.println("infId : " +infId);
	          
	   	   description = jsonObject.getString("description");
	   	   
	   	   System.out.println("description : ***************" +description);
	        
	    } catch (Exception e) {
	        e.printStackTrace();
	    }
		
		if (extras != null)
		{
			// if we are in the foreground, just surface the payload, else post it to the statusbar
            if (PushPlugin.isInForeground()) {
				extras.putBoolean("foreground", true);
                PushPlugin.sendExtras(extras);
				
				 if(title.equalsIgnoreCase("ThanksGiving") || title.equalsIgnoreCase("PrayerRequest")){
                	
                }
                else{
                //createNotification(context, extras); //Get notification on status bar
                }
				
			}
			else {
				 if (extras.getString("message") != null && extras.getString("message").length() != 0) {
                	
                	//PushPlugin.sendExtras(extras);
                
                	
                	/* code to save push message in shared preferrences starts*/
                	
                	Context context1 = this;
                	JSONObject jsonObject1 = null;
					try {
						jsonObject1 = new JSONObject(message);
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
                	
                	insertData(context1, infId, jsonObject1 ); //calling insert data method 
                	
                	
                	/* code to save push message in shared preferrences starts*/
                	
                	
                	if(title.equalsIgnoreCase("ThanksGiving") || title.equalsIgnoreCase("PrayerRequest")){
                    	
                    }
                    else{
                    createNotification(context, extras); //Get notification on status bar
                    }
                }
            }
        }
	}

	public void createNotification(Context context, Bundle extras)
	{
		NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
		String appName = getAppName(this);

		Intent notificationIntent = new Intent(this, PushHandlerActivity.class);
		notificationIntent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
		notificationIntent.putExtra("pushBundle", extras);

		PendingIntent contentIntent = PendingIntent.getActivity(this, 0, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);
		
		int defaults = Notification.DEFAULT_ALL;

		if (extras.getString("defaults") != null) {
			try {
				defaults = Integer.parseInt(extras.getString("defaults"));
			} catch (NumberFormatException e) {}
		}
		
		NotificationCompat.Builder mBuilder =
			new NotificationCompat.Builder(context)
				.setDefaults(0) /*setDefaults(defaults) is changed for silet sound and no vibration when
				   push notfication comes by kiran*/
				.setSmallIcon(context.getApplicationInfo().icon)
				.setWhen(System.currentTimeMillis())
				.setContentTitle(extras.getString("title"))
				.setTicker(extras.getString("title"))
				.setContentIntent(contentIntent)
				.setAutoCancel(true);

		String message = extras.getString("message");
		
	
		
		if (message != null) {
			mBuilder.setContentTitle(title);
			mBuilder.setContentText(description);
		} else {
			mBuilder.setContentText("<missing message content>");
		}

		String msgcnt = extras.getString("msgcnt");
		if (msgcnt != null) {
			mBuilder.setNumber(Integer.parseInt(msgcnt));
		}
		
		int notId = 0;
		
		try {
			notId = Integer.parseInt(extras.getString("notId"));
		}
		catch(NumberFormatException e) {
			Log.e(TAG, "Number format exception - Error parsing Notification ID: " + e.getMessage());
		}
		catch(Exception e) {
			Log.e(TAG, "Number format exception - Error parsing Notification ID" + e.getMessage());
		}
		
		mNotificationManager.notify((String) appName, notId, mBuilder.build());
	}
	
	private static String getAppName(Context context)
	{
		CharSequence appName = 
				context
					.getPackageManager()
					.getApplicationLabel(context.getApplicationInfo());
		
		return (String)appName;
	}
	
	@Override
	public void onError(Context context, String errorId) {
		Log.e(TAG, "onError - errorId: " + errorId);
	}
	
	public void insertData(Context context, String pKey1, JSONObject jsonObject) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context.getApplicationContext());
        System.out.println("jsonObject #########################"+jsonObject);
        System.out.println("pKey1 ******************************"+pKey1);
         //prefs.edit().putString("key1", pKey1).commit();
         prefs.edit().putString(pKey1, jsonObject.toString()).commit();
    }


}